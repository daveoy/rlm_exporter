# example kubernetes manifests

be sure to edit your image registry and pullsecrets in deployment.yaml

you can generate the configmap from the json file as such:
```
kubectl create configmap rlm-servers --from-file=../conf/example.json --dry-run=client -o yaml > configmap.yaml
```