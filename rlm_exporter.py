#!/usr/bin/python3

from prometheus_client import start_http_server, Metric, REGISTRY
from threading import Thread
import time
import sys
import random
import platform
import getpass
import socket
import struct
import datetime
import json


def rs232_checksum(the_bytes):
    return sum(the_bytes) & 0xFF


def createDataPacket(databytes):
    header = bytearray(struct.pack("HHxx", 1, len(databytes)))
    buffer = header + databytes
    struct.pack_into("B", buffer, 4, rs232_checksum(buffer))
    struct.pack_into("B", buffer, 5, rs232_checksum(buffer[0:5]))
    return bytes(buffer)


class RLMCollector(object):
    def __init__(self, hostname, ports):
        self._hostname = hostname
        self._ports = ports
        self.handle = random.randint(10000, 65535)
        print("%04x" % self.handle)
        self.username = bytes(getpass.getuser(), "ascii")
        self.machine = bytes(socket.gethostname(), "ascii")
        self.command = bytes("stat - rlmutil", "ascii")
        self.os_platform = bytes("x64_l1", "ascii")
        self.network = bytes("7f0101 000000000000 ip=0.0.0.0 ", "ascii")
        self.extra = bytes("%04x,0,0,c,2,2,0,0,0,0" % self.handle, "ascii")
        self.kernel = bytes(f"{platform.uname().release}>./rlmutil", "ascii")
        Thread.__init__(self)
        self.daemon = True
        self.collect()

    def collect(self):
        issuedmetric = Metric('rlm_feature_issued', 'License feature issued labeled by app and feature name of the license', 'gauge')
        usedmetric = Metric('rlm_feature_used', 'License feature issued labeled by app and feature name of the license', 'gauge')
        usedusersmetric = Metric('rlm_feature_used_users', 'License feature issued labeled by app and feature name of the license', 'gauge')

        for port in self._ports:
            with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
                s.connect((self._hostname, int(port)))
                data = createDataPacket(struct.pack("<Hxbxx%dsx%dsx%dsx%dsx%dsxx2sx%dsx%ds6xxx" % (len(self.username), len(self.machine), len(self.command), len(self.os_platform), len(self.network), len(self.extra), len(self.kernel)),
                                                    12631, 50, self.username, self.machine, self.command, self.os_platform, self.network, bytes("17", "ascii"), self.extra, self.kernel))

                s.sendall(data)
                result = s.recv(1024)

                newdata = createDataPacket(b'\x59\x00\x00')
                s.sendall(newdata)
                result = s.recv(1024)
                s.close()
                array = result[4:].split(b'\x00')
                offset = 20
                vendorcommand = bytes("-nolog-", "ascii")
                while array[offset] != b'':
                    daemon = array[offset][1:]
                    port, = struct.unpack(">H", bytes.fromhex(array[offset + 1].decode("ascii")))

                    offset += 6

                    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as vendorsocket:
                        vendorsocket.connect((self._hostname, port))
                        data = createDataPacket(b'\x50\x00\x30\x2c\x30\x2c\x30\x00\x00\x00\x00\x00\x00')
                        vendorsocket.sendall(data)

                        vendordata = {}
                        flags = ""
                        while flags != "704c":
                            header = vendorsocket.recv(6)
                            command, totalsize, checksum = struct.unpack("HHH", header)
                            received = bytes()
                            while len(received) < totalsize:
                                received += vendorsocket.recv(totalsize - len(received))
                            flags = received[:2].hex()
                            data = received[2:-2].split(b'\x00')

                            numfeatures = int((len(data)) / 25)
                            for i in range(0, numfeatures):
                                featurename = data[i * 25 + 0].decode("ascii")
                                featuredata = {'inuse': vendordata.get(featurename, {}).get('inuse', 0), 'avail': vendordata.get(featurename, {}).get('avail', 0), 'users': [], 'checkouts': []}
                                featureversion = data[i * 25 + 1].decode("ascii")
                                featureavail = int(data[i * 25 + 6].decode("ascii"), 16)
                                featureinuse = int(data[i * 25 + 8].decode("ascii"), 16)
                                featurelicensestartdate, featurelicenseexpiredate = data[i * 25 + 13].decode("ascii").split(",")
                                if featurelicensestartdate != "":
                                    featurelicensestartdate = datetime.datetime.strptime(featurelicensestartdate, "%d-%b-%Y")
                                else:
                                    featurelicensestartdate = datetime.datetime.strptime("01-Jan-2000", "%d-%b-%Y")
                                if featurelicenseexpiredate == "permanent":
                                    # Force a specific date for permanent licenses
                                    featurelicenseexpiredate = "01-jan-2037"
                                featurelicenseexpiredate = datetime.datetime.strptime(featurelicenseexpiredate, "%d-%b-%Y")

                                numfeaturecheckouts = int(data[i * 25 + 10].decode("ascii"), 16)
                                issuedmetric.add_sample("rlm_feature_issued", value=featureavail, labels={'name': featurename, 'version': featureversion, 'start': featurelicensestartdate.strftime('%Y/%m/%d %H:%M:%S'), 'expire': featurelicenseexpiredate.strftime('%Y/%m/%d %H:%M:%S'), 'server': self._hostname})
                                featuredata['avail'] += featureavail

                                vendordata[featurename] = featuredata

                        data = createDataPacket(b'\x75\x00\x00\x00\x00\x00\x00\x00')
                        vendorsocket.sendall(data)

                        flags = ""
                        usedusersmetricdata = {}
                        while flags != "554c":
                            header = vendorsocket.recv(6)
                            if len(header) == 6:
                                command, totalsize, checksum = struct.unpack("HHH", header)
                                received = bytes()
                                while len(received) < totalsize:
                                    received += vendorsocket.recv(totalsize - len(received))
                                flags = received[:2].hex()
                                resultarray = received[2:-2].split(b'\x00')
                                usercount = int(len(resultarray) / 13)
                                for i in range(0, usercount):
                                    featurename = resultarray[i * 13 + 0].decode("ascii")
                                    featureusername = resultarray[i * 13 + 1].decode("ascii")
                                    featuremachine = resultarray[i * 13 + 2].decode("ascii")
                                    featureversion = resultarray[i * 13 + 6].decode("ascii")
                                    featurepool = int(resultarray[i * 13 + 12].decode("ascii"), 16)
                                    checkout = "%s@%s@%s@%s" % (featureusername, featuremachine, featureversion, featurepool)
                                    if checkout not in vendordata[featurename]['checkouts']:
                                        # Make sure there's only 1 entry per license checkout
                                        usedusersmetricdata[f"{featurename}_{featureusername}_{featuremachine}_{self._hostname}_{featurepool}"] = {'name': featurename, 'user': featureusername, 'machine': featuremachine, 'version': featureversion, 'pool': str(featurepool), 'server': self._hostname}
                            else:
                                print("Header wrong size ", header.hex())
                        for name, item in usedusersmetricdata.items():
                            usedusersmetric.add_sample("rlm_feature_used_users", value=1, labels={'name': item['name'], 'user': item['user'], 'machine': item['machine'], 'version': item['version'], 'pool': item['pool'], 'server': item['server']})

                        for featurename, featuredata in vendordata.items():
                            usedmetric.add_sample('rlm_feature_used', value=featuredata.get('inuse', 0), labels={'name': featurename, 'server': self._hostname})

                        vendorsocket.close()
        print(issuedmetric,usedmetric,usedusersmetric)
        yield issuedmetric
        yield usedmetric
        yield usedusersmetric

if __name__ == '__main__':
    confFile = open(sys.argv[1])
    conf = json.load(confFile)
    confFile.close()
    start_http_server(conf['listen_port'])
    for _server in conf['servers']:
        print(_server)
        REGISTRY.register(RLMCollector(_server['server'], _server['server_ports']))
    while True:
        time.sleep(1)
