# Description

This is a simple prometheus exporter for collecting usage stats of licenses on an RLM License server. It connects directly to the license server and does not require `rlmutil` installed.

# Dependencies

Python 3
prometheus_client (python module)

# Usage

```bash
rlm_exporter.py conf/example.json
```

# docker
be sure to build the container against the platform on which you intend it to run, this uses rlm_exporter_env.py
```
docker build . -t rlm_exporter
docker run --rm -it -p 9701:9701 -v `pwd`/conf:/conf rlm_exporter /conf/example.json
```

# gitlab CI
see .gitlab-ci.yml for details

# kubernetes
the kube folder contains example manifests for deploying the container built from this repo to kubernetes:

1. configmap for your environment's config
2. pod spec which will mount the configmap
3. service for prometheus to scrape
4. servicemonitor to configure prometheus to scrape

# Dashboard

Import the dashboard.json in Grafana

![dashboard](dashboard.png)
